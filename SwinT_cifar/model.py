_model_entrypoints = {}


def register_model(fn):
    _model_entrypoints[fn.__name__] = fn
    return fn


def build_model(model_name, **kwargs):
    return _model_entrypoints[model_name](**kwargs)

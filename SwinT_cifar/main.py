"""
python main.py --model swin_s --dataset cifar10 --sched cosine --epochs 200 --lr 0.01 --gpu 1 --root ~/datasets
s,t,b,l
"""
import argparse
import logging
import os
import sys
import pandas as pd
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import yaml
import swin_transformer
from dataset import build_dataloader
from model import build_model
from mutator import build_mutator
from set_args import parse_args
from train_valid import train_one_epoch, validate
from utils import Timer, build_expname, count_params

USE_HYPERBOX = True


def main():
    cudnn.benchmark = True
    cudnn.deterministic = True

    args = parse_args()

    # process argparse & yaml
    if not args.config:
        opt = vars(args)
        args = yaml.load(open(args.config), Loader=yaml.FullLoader)
        opt.update(args)
        args = opt
    else:  # yaml priority is higher than args
        opt = yaml.load(open(args.config), Loader=yaml.FullLoader)
        opt.update(vars(args))
        args = argparse.Namespace(**opt)

    args.name = build_expname(args)

    if not os.path.exists("exps/%s" % args.name):
        os.makedirs("exps/%s" % args.name)

    print("--------Config -----")
    for arg in vars(args):
        print("%s: %s" % (arg, getattr(args, arg)))
    print("--------------------")

    with open("exps/%s/args.txt" % args.name, "w") as f:
        for arg in vars(args):
            print("%s: %s" % (arg, getattr(args, arg)), file=f)

    log_format = "%(asctime)s %(message)s"
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format=log_format,
        datefmt="%m/%d %I:%M:%S %p",
    )

    fh = logging.FileHandler(os.path.join("exps", args.name, "log.txt"))
    fh.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(fh)
    logging.info(args)

    #######################################################################

    train_loader = build_dataloader(args.dataset, type="train", args=args)
    test_loader = build_dataloader(args.dataset, type="val", args=args)

    # create model
    num_classes = 10 if args.dataset == "cifar10" else 100
    if not USE_HYPERBOX:
        model = build_model(args.model, num_classes=num_classes)
        mutator = None
        logging.info(f"param of model {args.model} is {count_params(model)}")
    else:
        model = build_model(args.model, num_classes=num_classes)
        mutator = build_mutator("random", model)

    # stat(model, (3, 32, 32))
    # from torchsummary import summary
    # summary(model, input_size=(3, 32, 32), batch_size=-1)

    model = model.cuda()

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(
        filter(lambda p: p.requires_grad, model.parameters()),
        lr=args.lr,
        momentum=args.momentum,
        weight_decay=args.weight_decay,
    )
    scheduler = torch.optim.lr_scheduler.MultiStepLR(
        optimizer,
        milestones=[int(e) for e in args.milestones.split(",")],
        gamma=args.gamma,
    )

    log = pd.DataFrame(
        index=[], columns=["epoch", "lr", "loss", "acc", "val_loss", "val_acc"]
    )

    logging.info("Training Start...")

    timer = Timer()

    best_acc = 0
    for epoch in range(args.epochs):
        # logging.info("Epoch [%03d/%03d]" % (epoch, args.epochs))
        # train for one epoch
        train_log = train_one_epoch(
            args,
            train_loader,
            model,
            criterion,
            optimizer,
            epoch,
            scheduler=scheduler,
            mutator=mutator,
        )

        train_time = timer()

        # evaluate on validation set
        val_log = validate(args, test_loader, model, criterion, epoch)

        scheduler.step()

        logging.info(
            f"Epoch:[{epoch:03d}/{args.epochs:03d}] lr:{scheduler.get_last_lr()[0]:.4f} train_acc:{train_log['acc']/100.:.3f} train_loss:{train_log['loss']:.4f} train_time:{train_time:03.2f} valid_acc:{val_log['acc']/100.:.3f} valid_loss:{val_log['loss']:.4f} valid_time:{timer():03.2f} total_time: {timer()+train_time:.2f}"
        )

        tmp = pd.Series(
            [
                epoch,
                scheduler.get_last_lr()[0],
                train_log["loss"],
                train_log["acc"],
                val_log["loss"],
                val_log["acc"],
            ],
            index=["epoch", "lr", "loss", "acc", "val_loss", "val_acc"],
        )

        log = log.append(tmp, ignore_index=True)
        log.to_csv("exps/%s/log.csv" % args.name, index=False)

        if val_log["acc"] > best_acc:
            from glob import glob

            useless_files = glob("exps/%s/*.pth" % args.name)
            for file in useless_files:
                os.remove(file)
            torch.save(
                model.state_dict(),
                "exps/%s/model_%d.pth" % (args.name, (val_log["acc"] * 100)),
            )
            best_acc = val_log["acc"]


if __name__ == "__main__":
    main()
